var DSNV = "DSNV";
var dsnv = [];




// lấy dữ liệu từ localStorage
var dsnvJson = localStorage.getItem(DSNV);
// convert json to array
if (dsnvJson != null) {
  var nvArr = JSON.parse(dsnvJson);

  dsnv = nvArr.map(function (item) {
    return new NhanVien(
      item.tknv,
      item.name,
      item.email,
      item.password,
      item.date,
      item.luong,
      item.chucvu,
      item.giolam
    );
  });
  renderDSNV(dsnv);
}
// thêm nhân viên
function themNhanVien() {
  var nv = layThongTinTuForm();
  console.log("🚀 ~ file: main.js:27 ~ themNhanVien ~ nv", nv)
  
  // validate
  var isValid = true;  
  // kiểm tra mã nv
  isValid =kiemTraEmpty("tknv", "tbTKNV")&&kiemTraTrung(nv.tknv, dsnv)&&kiemTraSo(nv.tknv, "tbTKNV") &&kiemTraDoDai(nv.tknv, "tbTKNV", 4, 6) ;
  // kiểm tra tên
  isValid = isValid & kiemTraEmpty("name", "tbTen") && kiemTraChu(nv.name, "tbTen");
  // kiểm tra email
  isValid = isValid & kiemTraEmpty("email", "tbEmail") && kiemTraEmail(nv.email);
  // kiểm tra password
  isValid = isValid & kiemTraEmpty("password", "tbMatKhau") &&  kiemTraPassword(nv.password);
  // kiểm tra date
  isValid = isValid & kiemTraEmpty("datepicker", "tbNgay") &&  kiemTraDate(nv.date);
  // kiểm tra lương
  isValid = isValid & kiemTraEmpty("luongCB", "tbLuongCB") &&  kiemTraLuong(nv.luong);
  // kiểm tra chức vụ
  isValid = isValid & kiemTraChucVu(nv.chucvu, "tbChucVu");
  // kiểm tra giờ làm
  isValid = isValid & kiemTraEmpty("gioLam", "tbGiolam") && kiemTraGio(nv.giolam);

  

  if (isValid) {
    document.getElementById('btnThemNV').setAttribute('data-dismiss','modal')

    // nếu isValid true thì tiến hành thêm nv
    dsnv.push(nv);
    // lưu vào localStorage
    var dsnvJson = JSON.stringify(dsnv);
    localStorage.setItem(DSNV, dsnvJson);
    renderDSNV(dsnv);
  }
}
// xoá nhân viên
function xoaNV(idNv) {
  var viTri = timViTri(idNv, dsnv);
  if (viTri != -1) {
    dsnv.splice(viTri, 1);
    var dsnvJson = JSON.stringify(dsnv);
    localStorage.setItem(DSNV, dsnvJson);
    renderDSNV(dsnv);
  }
}

// sửa nv

function suaNV(idNv) {
  var viTri = timViTri(idNv, dsnv);
  if (viTri == -1) {
    return;
  }
  var nv = dsnv[viTri];
  showThongTinLenForm(nv);
}

function capNhatNV() {  
  var nv = layThongTinTuForm();
  // validate
  var isValid = true;  
  // kiểm tra mã nv
  isValid =kiemTraEmpty("tknv", "tbTKNV")&&kiemTraSo(nv.tknv, "tbTKNV") &&kiemTraDoDai(nv.tknv, "tbTKNV", 4, 6) ;
  // kiểm tra tên
  isValid = isValid & kiemTraEmpty("name", "tbTen") && kiemTraChu(nv.name, "tbTen");
  // kiểm tra email
  isValid = isValid & kiemTraEmpty("email", "tbEmail") && kiemTraEmail(nv.email);
  // kiểm tra password
  isValid = isValid & kiemTraEmpty("password", "tbMatKhau") &&  kiemTraPassword(nv.password);
  // kiểm tra date
  isValid = isValid & kiemTraEmpty("datepicker", "tbNgay") &&  kiemTraDate(nv.date);
  // kiểm tra lương
  isValid = isValid & kiemTraEmpty("luongCB", "tbLuongCB") &&  kiemTraLuong(nv.luong);
  // kiểm tra chức vụ
  isValid = isValid & kiemTraChucVu(nv.chucvu, "tbChucVu");
  // kiểm tra giờ làm
  isValid = isValid & kiemTraEmpty("gioLam", "tbGiolam") && kiemTraGio(nv.giolam);
  
  // update dữ diệu mới 
  if (isValid) {
    document.getElementById('btnCapNhat').setAttribute('data-dismiss','modal')
    var viTri = timViTri(nv.tknv, dsnv);
    if (viTri != -1) {
      dsnv[viTri] = nv;
      var dsnvJson = JSON.stringify(dsnv);
      localStorage.setItem(DSNV, dsnvJson);

      renderDSNV(dsnv);
    }
  }

}
