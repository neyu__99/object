function NhanVien(
    _tknv,
    _name,
    _email,
    _password,
    _date,
    _luong,
    _chucvu,
    _giolam  
    ) {
    this.tknv = _tknv;
    this.name = _name;
    this.email = _email;
    this.password = _password;
    this.date = _date;
    this.luong = _luong*1;
    this.chucvu = _chucvu;
    this.giolam = _giolam*1;
    this.tinhTongLuong = function () {
        if (this.chucvu== "Sếp"){
            return this.luong*3
        }
        else if (this.chucvu== "Trưởng phòng"){
            return this.luong*2
        }
        else if (this.chucvu== "Nhân viên"){
            return this.luong*1
        }
    };
    this.xepLoai = function () {
        if (this.giolam>=192){
            return 'xuất sắc'
        }
        else if (this.giolam>=176){
            return 'giỏi'
        }
        else if (this.giolam>=160){
            return 'khá'
        }
        else return 'trung bình'
    };
  }
  // function layThongTinTuFormMoi() {
  //   var arr=document.querySelectorAll('.modal-body input')
  //   for (var index = 0; index < arr.length; index++) {
  //     var content = arr[index].value;
  //     console.log("🚀 ~ file: nvcontroller.js:47 ~ layThongTinTuFormMoi ~ content", content)
  //     content=''
  //     // content.empty()
      
  //   }
  //   const _tknv = document.getElementById("tknv").value;
  //   const _name = document.getElementById("name").value;
  //   const _email = document.getElementById("email").value;
  //   const _password = document.getElementById("password").value;
  //   const _date = document.getElementById("datepicker").value;
  //   const _luong = document.getElementById("luongCB").value;
  //   const _chucvu = document.getElementById("chucvu").value;   
  //   const _giolam = document.getElementById("gioLam").value;
    
  //   var nv = new NhanVien(
  //       _tknv,
  //       _name,
  //       _email,
  //       _password,
  //       _date,
  //       _luong,
  //       _chucvu,
  //       _giolam
  //   );
  //   return nv;
  // }

  function layThongTinTuForm() {
    const _tknv = document.getElementById("tknv").value;
    const _name = document.getElementById("name").value;
    const _email = document.getElementById("email").value;
    const _password = document.getElementById("password").value;
    const _date = document.getElementById("datepicker").value;
    const _luong = document.getElementById("luongCB").value;
    const _chucvu = document.getElementById("chucvu").value;   
    const _giolam = document.getElementById("gioLam").value;
    
    var nv = new NhanVien(
        _tknv,
        _name,
        _email,
        _password,
        _date,
        _luong,
        _chucvu,
        _giolam
    );
    return nv;
  }
  
  function renderDSNV(nvArr) {
    var contentHTML = "";
    for (var index = 0; index < nvArr.length; index++) {
      var item = nvArr[index];
      var contentTr = `<tr>
                           <td>${item.tknv}</td>
                          <td>${item.name}</td>
                          <td>${item.email}</td>
                          <td>${item.date}</td>
                          <td>${item.chucvu}</td>
                          <td>${item.tinhTongLuong()}</td>
                          <td>${item.xepLoai()}</td>
                          <td>
                           <button onclick="xoaNV('${item.tknv}')" class="btn btn-danger">Xoá</button>
  
                           <button id="SuaNhanVien" onclick="suaNV('${item.tknv}')" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Sửa</button>
                          </td>
                       </tr>`;
      contentHTML += contentTr;
    }
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
  }
  
  function timViTri(id, arr) {
    var viTri = -1;
    for (var index = 0; index < arr.length; index++) {
      var nv = arr[index];
      if (nv.tknv == id) {
        viTri = index;
        break;
      }
    }
    return viTri;
  }
  function showThongTinLenForm(nv) {
    document.getElementById("tknv").value = nv.tknv;
    document.getElementById("name").value = nv.name;
    document.getElementById("email").value = nv.email;
    document.getElementById("password").value = nv.password;
    document.getElementById("datepicker").value = nv.date;
    document.getElementById("luongCB").value = nv.luong;
    document.getElementById("chucvu").value = nv.chucvu;
    document.getElementById("gioLam").value = nv.giolam;
  }
    