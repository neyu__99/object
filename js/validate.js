function kiemTraEmpty(idNv, idErr) {
  var idvalue = document.getElementById(idNv).value;
  var idplaceholder = document.getElementById(idNv).placeholder;
  if (idvalue == "") {
    document.getElementById(idErr).innerHTML = `Vui lòng nhập ${idplaceholder}`;
    return false;
  }
  else {return true}
}
function kiemTraTrung(idNv, NvArr) {
    var index = NvArr.findIndex(function (item) {
      return idNv == item.tknv;
    });
    if (index == -1) {
      // ko tìm thấy
      // xoá thông báo lỗi nếu có
      document.getElementById("tbTKNV").innerHTML = "";
      return true;
    } else {
      // show thông báo cho user
      document.getElementById("tbTKNV").innerHTML = "Tài khoản đã tồn tại";
      return false;
    }
  }
  function kiemTraSo(value, idErr) {
    var reg = /^\d+$/;
    // test() , return true, false
    var isNumber = reg.test(value);
    if (isNumber) {
      document.getElementById(idErr).innerText = "";
      return true;
    } else {
      document.getElementById(idErr).innerText = "Phải nhập số";
      return false;
    }
  }

  function kiemTraDoDai(value, idErr, min, max) {
    var length = value.length;
    if (length < min || length > max) {
      document.getElementById(
        idErr
      ).innerText = `Độ dài phải từ ${min} đến ${max} kí tự`;
      return false;
    } else {
      document.getElementById(idErr).innerText = "";
      return true;
    }
  }
  function kiemTraChu(value, idErr) {
    var reg = /^[a-zA-Z]*$/;
    // test() , return true, false
    var isText = reg.test(value);
    if (isText) {
      document.getElementById(idErr).innerText = "";
      return true;
    } else {
      document.getElementById(idErr).innerText = "Phải nhập kí tự Alphabet";
      return false;
    }
  }

  function kiemTraEmail(value) {
    const reg =/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var isEmail = reg.test(value);
    if (isEmail) {
      document.getElementById("tbEmail").innerText = "";
      return true;
    } else {
      document.getElementById("tbEmail").innerText = "Email không hợp lệ";
      return false;
    }
  }
  function kiemTraPassword(value) {
    const reg =/^(?=.*\d)(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,10}$/;
    var isPassword = reg.test(value);
    if (isPassword) {
      document.getElementById("tbMatKhau").innerText = "";
      return true;
    } else {
      document.getElementById("tbMatKhau").innerText = "Mật Khẩu từ 6-10 ký tự và chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt";
      return false;
    }
  }
  function kiemTraDate(value) {
    const reg =/^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/;
    var isDate = reg.test(value);
    if (isDate) {
      document.getElementById("tbNgay").innerText = "";
      return true;
    } else {
      document.getElementById("tbNgay").innerText = "Ngày không hợp lệ";
      return false;
    }
  }
  function kiemTraLuong(value) {
    const reg =/^(100000[0-9]|10000[1-9][0-9]|1000[1-9][0-9]{2}|100[1-9][0-9]{3}|10[1-9][0-9]{4}|1[1-9][0-9]{5}|[2-9][0-9]{6}|1[0-9]{7}|20000000)$/;
    var isLuong = reg.test(value);
    if (isLuong) {
      document.getElementById("tbLuongCB").innerText = "";
      return true;
    } else {
      document.getElementById("tbLuongCB").innerText = "Lương từ 1.000.000 đến 20.000.000";
      return false;
    }
  }
  function kiemTraChucVu(value, idErr) {
    if (value=="Chọn chức vụ") {
      document.getElementById(idErr).innerText = "Vui lòng chọn chức vụ";
      return false;
    } else {
      document.getElementById(idErr).innerText = "";
      return true;
    }
  }

  function kiemTraGio(value) {
    const reg =/^(8[0-9]|9[0-9]|1[0-9]{2}|200)$/;
    var isGio = reg.test(value);
    if (isGio) {
      document.getElementById("tbGiolam").innerText = "";
      return true;
    } else {
      document.getElementById("tbGiolam").innerText = "Giờ từ 80 đến 200";
      return false;
    }
  }




  